const $ = require("jquery");
const uiKit = require("uikit");

//header scroll
$(window).scroll(function () {
    if ($(window).scrollTop() > 0) {
        $('.header-top').addClass('fixed')
        return false
    } else {
        $('.header-top').removeClass('fixed')
    }
});


//mobile menu
$(".header-mobile").click(function () {
    $(".mobile-menu").show();
});
$(".mobile-menu__nav__dot").click(function () {
    $(".mobile-menu").hide();
});
$(".mobile-menu__nav__list__item").click(function () {
    $(".mobile-menu").hide();
});

//overlay-contacts
$(".contacts").click(function () {
    $(".overlay-contacts").show();
});
$(".overlay-contacts__arrow").click(function () {
    $(".overlay-contacts").hide();
});
